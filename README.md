# Matrix

## Setup

### Cloning existing projects
In order to complete the first project you need to have access
to the files of the project locally on your machine. Now, GitLab has some handy
features that allow us to maintain our own versions or projects as well as easily
obtain them.

First, you need to make sure that you have a GitLab account. If you do not you
can register [here](https://gitlab.com/users/sign_in).

Next, ensure that you have git on your machine. Try executing `git --help` from
the command line. If the git command is not found, you'll need to install git with
your package manager. For Debian: `sudo apt install git`.

Now, you need to fork and then clone this project, to do this:

1. Click 'Fork' on the project's main page ([here](https://gitlab.com/ct-starter-guide/project1)).
2. Click on your namespace and notice that the suffix of the url has changed from *'ct-starter-guide/project1'*
  to *'<your_namespace>/project1'* as you are now on your fork.
3. Next to the fork button, you should see a box with 'HTTPS' and a url. Click on the
right button 'Copy URL to clipboard' and then from your command line execute:
`git clone https://gitlab.com/<your_namespace>/project1.git`
4. Now change into the directory of the first project:
`cd project1`

You should now be in the directory of the first project. You'll see that this project
contains:

1. README.md
  - Most repositories have a README.md, this is just a simple file summarises a repository
  or directory within a repository.
2. python/ directory
  - Skeleton implementation in python - this is where you should start
3. c/ directory
  - Skeleton in c - a lower level language than python, consider this a stretch goal

See the README.md files in these directories for specific instructions.

# The Problem

Given a string which represents a matrix of numbers, build a data structure that
allows easy access to the rows and columns of the matrix. For example, if you are
given the string: "9 8 7\n5 3 2\n6 6 7", this would produce the following matrix:

```text
9 8 7
5 3 2
6 6 7
```

Below illustrates how we can represent this matrix with indices:

```text
    0  1  2
  |---------
0 | 9  8  7
1 | 5  3  2
2 | 6  6  7
```

your code should be able to spit out:

- A list of the rows, reading each row left-to-right while moving
  top-to-bottom across the rows,
- A list of the columns, reading each column top-to-bottom while moving
  from left-to-right.

The rows for our example matrix:

- 9, 8, 7
- 5, 3, 2
- 6, 6, 7

And its columns:

- 9, 5, 6
- 8, 3, 6
- 7, 2, 7

Your functions must be able to return a list of a specified column or row.
